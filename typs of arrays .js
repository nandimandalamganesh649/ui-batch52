//push//
const animals = ['lion', 'tiger', 'deer'];
const count = animals.push('monkey');

console.log(animals);

const things = [ 'table', 'pencil', 'eraser'];
const things1 = things.push('mouse');

console .log(things);

//pop//
const fruits = [ 'apple', 'mango','grape','blackberry'];
const fruits2 = fruits.pop();
console.log(fruits2);

//shift//
const fruit = ['apple','mango','greeenapple','orange'];
const fruit1 = fruit.shift();
console.log(fruit1);

//unshift//
const fruite = ['appple','mango','orange'];
const fruite1 = fruite.unshift('kismiss','badam');
console.log(fruite);

//concat//
const prod = [ 'pencil', 'eraser'];
const prod1 = [ 'sharpner','scale'];
const prod2 = prod.concat(prod1);
console.log(prod2);

//slice//
const veg = [ 'app', 'mango','grape'];
const veg1 = veg.slice('1','3');
console.log(veg1);

// splice//
const vegi = [ 'app', 'mang','green'];
const vegie = vegi.splice('2','0','lemon','kiwi');
console.log(vegi); // 2,2 add2&rem2 2,1 add2&rem1//

//index of //
const fru = [ 'mang','blackberry','mang','oran'];
const fru1 = fru.indexOf('mang');
console.log(fru1); 

//last index of //
const frue = [ 'app','blackberry','app','oran'];
const frue1 = frue.lastIndexOf('app');
console.log(frue1);

//filter//

//map//
const nums = [ 4,9,25,36];
const newnums = nums.map(Math.sqrt);
console.log(newnums);

const numers = [ 4,3,2,1];
const numers1 = numers.map(myFunction);
function myFunction(numers){
    return numers*10
}
console.log(numers1);

//for each//
const fruity = ["apple", "orange", "cherry"];
fruity.forEach(myFunction);
console.log(fruity)

//reduce//
const prizes = [1,2,3];
const tprizes = prizes.reduce(myFunction);
function myFunction(total,prizes){
    return prizes-total;
}
console.log(tprizes);

//sort//
const fruitt = ["ditecoke","burger","cheese","apple"];
 fruitt1=fruitt.sort();
console.log(fruitt1)

const mem =[ 4,6,1,7];
mem1 = mem.sort();
console.log(mem1);

//reverse//
const raw = [ "tomato","carrot","onion","potato"];
const raw1 = raw.reverse();
console.log(raw1);
